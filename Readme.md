# Base Library

This repository is intended to have base working code to make a library with CMake.

This was made bacause at the time of writing (2017.05.16) I did not find a complete example of CMake for compiling a library with **gtest** and exporting the library as a **python module**.



# How to use

- Download this repository without cloning it

- remove the folder `vendors/googletest` and the `.gitmodules`

- do a:

  ```sh
  $ git clone https://github.com/google/googletest.git vendors/googletest
  ```
  or if you are in a git repository:

  ```sh
  $ git submodule add https://github.com/google/googletest.git vendors/googletest
  ```

- Modify the CMakeFile for your library

- Use cmake as usual

- use your build chain as usual (make, VS, ...)

- rename the file `myLibrary.pyd.dll` to `myLibrary.pyd`



# Python module import

Once you have your `myLibrary.pyd`, you can put it in the same folder as your script and do:

```python
import myLibrary

print(myLibrary.sayHello(11))
print(myLibrary.freeFunction(20))
```

