#include <gtest/gtest.h>

#include <myLibrary/myLibraryAPI.hpp>

TEST(TestCategory, NameOfTheTest) {
    myLibrary ml;
    EXPECT_EQ(42, ml.sayHello());
    EXPECT_FALSE(ml.sayHello() == 21);
}


TEST(FreeFunctionCategory, FreeFunctionTest) {
    EXPECT_EQ(42, freeFunction(42));
}

TEST(FreeFunctionCategory, FreeFuntionTestException) {
    EXPECT_ANY_THROW(freeFunction(1));
}