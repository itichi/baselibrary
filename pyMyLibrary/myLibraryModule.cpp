#include <Python.h>

#include <myLibrary/myLibraryAPI.hpp>

static PyObject * myLibrary_sayHello(PyObject *self, PyObject *args)
{
	myLibrary ml;

    const int *command;
	if (!PyArg_ParseTuple(args, "i", &command))
        return NULL;

    int ret = ml.sayHello();

    return Py_BuildValue("i", ret);
}

static PyObject * myLibrary_freeFunction(PyObject *self, PyObject *args)
{
    const int *command;
	if (!PyArg_ParseTuple(args, "i", &command))
        return NULL;

	int ret = freeFunction(*command);

    return Py_BuildValue("i", ret);
}


// Structure to define how the functions are presented in python
static PyMethodDef myLibrary_methods[] = {
	// The first property is the name exposed to python, the second is the C++ function name        
	{ "sayHello", (PyCFunction)myLibrary_sayHello, METH_VARARGS, nullptr },
	{ "freeFunction", (PyCFunction)myLibrary_freeFunction, METH_VARARGS, nullptr },

	// Terminate the array with an object containing nulls.
	{ nullptr, nullptr, 0, nullptr }
};

// Structure to define the module as Python
static PyModuleDef myLibrary_module = {
	PyModuleDef_HEAD_INIT,
	"myLibrary",                        // Module name
	"Provide access to myLibrary from C++",  // Module description
	0,
	myLibrary_methods                   // Structure that defines the methods
};


// Add a method that Python calls when it loads the module, which must be must be named PyInit_<module-name>
PyMODINIT_FUNC PyInit_myLibrary() {
	return PyModule_Create(&myLibrary_module);
}
