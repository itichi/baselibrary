#include <myLibrary/myLibraryAPI.hpp>

#include <iostream>
#include <exception>

myLibrary::myLibrary(void) {};

int myLibrary::sayHello(void) {
    std::cout << "Hello world" << std::endl;
    return 42;
}

int freeFunction(int param) {
	std::cout << "myparam: " << param << std::endl;

    return param;
}